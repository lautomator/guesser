from django.conf.urls import patterns

urlpatterns = patterns(
    '',
    (r'new_game', 'guesser.views.new_game'),
    (r'game_logic', 'guesser.views.game_logic'),
    (r'', 'guesser.views.index'),
)
