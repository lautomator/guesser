from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
import random


def make_a_guess(the_answer, the_guess):
    # assume that both the_answer and the_guess came from
    # sources that will give you a string.
    the_answer = int(the_answer)
    if the_guess is None:
        return "Make a guess"
    else:
        try:
            the_guess = int(the_guess)  # and do the type conversion
        except ValueError:
            return "yo man.  I need a number."

        if the_answer == the_guess:
            return "you guessed %s and you got it" % the_guess
        else:
            return "you guessed %s, you are just not right." % the_guess


def index(request):
    the_answer = request.session.get('the_answer', None)
    if the_answer is None:
        return redirect('/new_game')
    the_guess = request.POST.get("the_guess", None)
    message = make_a_guess(the_answer, the_guess)
    context = {
        "the_answer": the_answer,
        "message": message
    }

    return render(request, 'guesser/index.html', context)


def new_game(request):
    request.session["the_answer"] = random.randint(0, 100)
    return redirect('/')


def game_logic(request):
    the_answer = request.session.get('the_answer', None)
    the_guess = request.POST.get("the_guess", None)
    message = make_a_guess(the_answer, the_guess)

    return HttpResponse(message)
